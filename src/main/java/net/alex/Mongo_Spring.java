package net.alex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mongo_Spring {
    public static void main(String[] args) {
        SpringApplication.run(Mongo_Spring.class, args);
    }
}
